# times-table

Idk how to describe this, I guess this is a geometric circular modular times table? o-o

## Controls
- `Enter/Space/m`: Increment multiplier
- `k`: Decrement multiplier
- `n`: Increment number of divisions
- `j`: Decrement number of divisions


### TODO

- [X] Add UI
- [ ] Make README more descriptive
