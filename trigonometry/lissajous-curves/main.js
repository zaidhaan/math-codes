window.onload = () => {
  const canvas = document.getElementById("canvas")
  let ctx = canvas.getContext("2d")
  let width = canvas.width = window.innerWidth
  let height = canvas.height = window.innerHeight

  let {PI} = Math

  let strokes = []
  let strokeProps = {trailSize: 315} // Made into an object so dat.GUI can access it

  let x = {
    center: width/2,
    radius: 200,
    angle: 0,
    speed: .1,
    val: 0
  }

  let y = {
    center: height/2,
    radius: 300,
    angle: 0,
    speed: .12,
    val: 0
  }

  let gui = new dat.GUI()
  let f1 = gui.addFolder("X Axis")
  f1.add(x, "radius", 1, 500)
  f1.add(x, "angle", 0, 2*PI)
  f1.add(x, "speed", 0, 0.99)
  f1.open()

  let f2 = gui.addFolder("Y Axis")
  f2.add(y, "radius", 1, 500)
  f2.add(y, "angle", 0, 2*PI)
  f2.add(y, "speed", 0, 0.99)
  f2.open()

  let trailController = gui.add(strokeProps, "trailSize", 1, 500)

  trailController.onFinishChange(val => {
    strokes = strokes.slice(0, val)
  })

  render()

  function render(){
    ctx.clearRect(0, 0, width, height)
    x.val = x.center + Math.sin(x.angle) * x.radius
    y.val = y.center + Math.sin(y.angle) * y.radius

    if(strokes.length > 0) {
      if(strokes.length + 1 > strokeProps.trailSize) strokes.shift()
      for (let i = 0; i < strokes.length; i++) {
        ctx.beginPath()
        ctx.moveTo(strokes[i].x, strokes[i].y)
        if(strokes[i+1]){
          ctx.lineTo(strokes[i+1].x, strokes[i+1].y)
        }else{
          ctx.lineTo(x.val, y.val)
        }
        ctx.stroke()
      }
      strokes.push({
        x: x.val,
        y: y.val
      })
    } else {
      strokes.push({
        x: x.val,
        y: y.val
      })
    }

    ctx.beginPath()
    ctx.arc(x.val, y.val, 5, 0, 2*PI)
    ctx.fill()

    x.angle += x.speed
    y.angle += y.speed

    window.requestAnimationFrame(render)
  }
}
